import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Test2 {
	public static List<String> readTxtFile(String filePath) {
		try {
			List<String> list=new ArrayList<String>();
			String encoding = "GBK";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					if (lineTxt.indexOf("2012")>0||lineTxt.indexOf("2013")>0||lineTxt.indexOf("2014")>0) {
						list.add(lineTxt);
					}
					//System.out.println(lineTxt);
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
			return list;
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String filePath = "c:\\src3.csv";
		List<String> allData = readTxtFile(filePath);
		for (int i = 0; i < allData.size(); i++) {
			System.out.println(allData.get(i));
		}
		
		List<Double> allPriceDoubles=getPrices(4.0,8.0,0.05);
		BP bp = new BP(6, 5, allPriceDoubles.size()/*,0.75,0.3*/);
		//准备工作完毕
		for (int i = 0; i != 2000; i++) {
			for (String value : allData) {
				String[] array=value.split(",");
				double[] real = new double[allPriceDoubles.size()];
				int index=getRealIndexArray(allPriceDoubles,Double.parseDouble(array[7]));
				real[index]=1;
				double[] binary = new double[6];
				binary[0]=Double.parseDouble(array[1]);
				binary[1]=Double.parseDouble(array[2]);
				binary[2]=Double.parseDouble(array[3]);
				binary[3]=Double.parseDouble(array[4]);
				binary[4]=Double.parseDouble(array[5]);
				binary[5]=Double.parseDouble(array[6]);
				if(Double.parseDouble(array[7])>0)
				bp.train(binary, real);
			}
		}
		
		System.out.println("训练完毕，下面请输入一组数字，神经网络将自动预测判断。");

		double[] binary = new double[6];
		binary[0]=7.19;
		binary[1]=7.25;
		binary[2]=7.14;
		binary[3]=7.22;
		binary[4]=3019933;
		binary[5]=21726328;
		double[] result = bp.test(binary);
		double max = -Integer.MIN_VALUE;
		int idx = -1;
		for (int i = 0; i != result.length; i++) {
			System.out.println(result[i]);
			if (result[i] > max) {
				max = result[i];
				idx = i;
			}
		}
		System.out.println("明日均价为：("+idx+")"+allPriceDoubles.get(idx)+"\n");
		
		/*binary[0]=7.18;
		binary[1]=7.27;
		binary[2]=7.09;
		binary[3]=7.19;
		binary[4]=3606041;
		binary[5]=25864760;
		result = bp.test(binary);
		System.out.println("08.15明日均价为："+result[0]+"\n");*/
	}

	private static int getRealIndexArray(List<Double> allPriceDoubles, double d) {
		int count=0;
		for (int i = 0; i < allPriceDoubles.size(); i++) {
			if(allPriceDoubles.get(i)<d){
				count++;
			}else {
				break;
			}
		}
		return count;
	}

	private static List<Double> getPrices(double d, double e, double i) {
		List<Double> allPriceDoubles=new ArrayList<Double>();
		do {
			d=d+i;
			allPriceDoubles.add(d);
		} while (d<e);
		return allPriceDoubles;
	}
	
}
